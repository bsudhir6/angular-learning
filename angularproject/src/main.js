console.log("This is main.ts file message by sudhir");
//print first five numbers
for (var i = 1; i <= 5; i++) {
    console.log(i);
}
//console.log("Value of i after FOR loop: ", i)
//anonymous function
var m = function () {
    return "JPMC";
};
console.log(m());
var p = function () { return "HYD"; };
console.log(p());
//Explicit Type Declaration - Type Annotation
var v1;
//v1 = "true";
v1 = "JPMC, HYD";
function sum(a, b) {
    return a + b;
}
var n = sum(3, 5);
console.log(n);
//Implicit Type Declaration - Type Inference
var v2 = 80;
function display() {
    console.log("This is Display Function");
}
