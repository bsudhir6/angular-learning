console.log("This is main.ts file message by sudhir")

//print first five numbers
for(let i = 1; i <= 5; i++){
  console.log(i)
}

//console.log("Value of i after FOR loop: ", i)

//anonymous function
let m = function(){
  return "JPMC"
}
console.log(m())

let p = () => "HYD"

console.log(p())

//Explicit Type Declaration - Type Annotation
let v1:string;
//v1 = "true";
v1 = "JPMC, HYD"

function sum(a:number , b:number):number{
  return a+b;
}
let n:number = sum(3, 5)
console.log(n)

//Implicit Type Declaration - Type Inference
let v2 = 80

function display(){
  console.log("This is Display Function")
}