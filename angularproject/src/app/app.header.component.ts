import { Component } from "@angular/core";
import { LoginService } from './shared/services/login.service';

@Component({
    selector: "app-header",
    // template: "<h4>Welcome to Shopping Site<h4>"
    templateUrl: "./header.component.html"
})
export class AppHeader{
    constructor(private lsvs:LoginService){
        console.log("App Header Component Constructor.")
    }
    isAdminLoggedIn():boolean{
        return this.lsvs.getIsAdminLoggedIn()
    }
}