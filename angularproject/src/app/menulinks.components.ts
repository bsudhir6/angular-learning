import { Component } from "@angular/core";
import { Category } from './models/category.model';
import { CategoryService } from './shared/services/category.service';

@Component({
    selector: "app-home",
    template: `<h1>Welcome to My Shopping Application</h1>
                <h3 [innerHTML]="myinfo"></h3>
                <h4> The Count value is {{ count }}</h4>
                <input type="number" value="{{ count }}" />
                <input type="number" [value]="count" />
                <button class="btn btn-primary" (click) = "incrementCount()">Increase</button>
                <input type="number" [(ngModel)]="count" />`
})
export class HomeComponent{
    myinfo = "<u>My Sample Info Content</u>"
    count=0;

    incrementCount(){
        this.count = this.count + 1;
    }

}

@Component({
    selector: "app-list",
    template: `
    <div class="row">
        <div class="col-sm-2 card">
            <div class="card-header bg-info"><b>Categories</b></div>
            <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item" *ngFor="let link of ctgLinks">
                        <a [routerLink]="link.id">{{link.name}}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-sm-5">
            <!--<app-products></app-products>-->
            <router-outlet></router-outlet>
        </div>
        <div class="col-sm-5 card">
            <app-cartitems></app-cartitems>
        </div>
    </div>    
    `
})
export class ListComponent {
    ctgLinks:Category[] = []
    constructor(private ctgsvc:CategoryService){}

    ngOnInit(){
        this.ctgsvc.getCategories().subscribe(
            (data) => {
                console.log("Success ", data)
                this.ctgLinks = data
            },(err) => {
                console.log("Error ", err)
            }
        )
    }
}

@Component({
    selector: "not-found",
    template: `<h2> View Not Found</h2>`
})
export class NotFoundComponent {}