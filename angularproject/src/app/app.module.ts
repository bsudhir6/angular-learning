import { NgModule } from "@angular/core"
import { AppHeader } from './app.header.component';
import { AppComponent } from './app.component';
import { BrowserModule } from "@angular/platform-browser"
import { LoginModule } from './login/login.module';
import { ShoppingModule } from './shopping/shopping.module';
import { HomeComponent, ListComponent, NotFoundComponent } from './menulinks.components';
import { Routes, RouterModule } from "@angular/router"
import { FormsModule } from "@angular/forms"
import { ProductService } from './shared/services/product.service';
import { CartService } from './shared/services/cart.service';
import { CategoryService } from './shared/services/category.service';
import { HttpClientModule } from "@angular/common/http"
import { ProductsComponent } from './shopping/products.component';
import { LoginService } from './shared/services/login.service';
import { LoginGuard } from './shared/services/login.guard';

let appRoutes:Routes = [{path: "home", component: HomeComponent},{path: "list", component: ListComponent, children:[
    {path:"", component:ProductsComponent},
    {path:":ctgId", component: ProductsComponent}
    ]},
    {path: "login", loadChildren:"./login/login.module#LoginModule"},
    {path:"", redirectTo:"home", pathMatch:'full'}, {path: "**", component:NotFoundComponent}]

@NgModule({
    //Register Components
    declarations: [ AppComponent, AppHeader, HomeComponent, ListComponent, NotFoundComponent ],
    //Startup Component Information
    bootstrap: [AppComponent],
    //Moduel Dependencies
    imports: [BrowserModule, ShoppingModule, RouterModule.forRoot(appRoutes, {useHash: true}), FormsModule, HttpClientModule],
    // Register Services
    providers: [ProductService, CartService, CategoryService, LoginService, LoginGuard]
})
export class AppModule{
    constructor(){
        console.log("App module Constructer.")
    }

}