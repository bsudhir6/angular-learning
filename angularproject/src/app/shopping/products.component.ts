import { Component, OnInit } from '@angular/core';
import { ProductService } from '../shared/services/product.service';
import { Product } from '../models/product.model';
import { CartService } from '../shared/services/cart.service';
import { CartItem } from '../models/cartitem.modle';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styles: []
})
export class ProductsComponent implements OnInit {
  myDate =  new Date() 
  products:Product[] = []
  queryString = ""
  constructor(private psvc:ProductService, private csvc:CartService, private ar:ActivatedRoute) { }

  addToCart(selectedProduct:Product){
    let myItem = new CartItem(selectedProduct.id, selectedProduct.name, selectedProduct.price, 1)
    this.csvc.addCartItem(myItem)
  }

  ngOnInit() {
    //this.products = this.psvc.getProducts()
    this.ar.params.subscribe((paramdata)=> {
      let paramId = paramdata.ctgId;
      if(paramId == undefined){
        this.products = this.psvc.getProducts()
      }else{
        this.products = this.psvc.getProducts().filter((e)=>e.categoryId == paramId)
      }
    })
  }
}