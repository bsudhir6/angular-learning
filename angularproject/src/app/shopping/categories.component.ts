import { Component, OnInit } from '@angular/core';
import { Category } from '../models/category.model';
import { CategoryService } from '../shared/services/category.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styles: []
})
export class CategoriesComponent implements OnInit {
  categories:Category[] = []
  frmCtg:Category = new Category(null, null)
  constructor(private ctgsvc:CategoryService) { }

  ngOnInit() {
    this.ctgsvc.getCategories().subscribe((data)=>this.categories = data, (err) => console.log("Error", err))
  }

  save(){
    if(this.frmCtg.id == null){
      this.ctgsvc.addCategory(this.frmCtg).subscribe(
        (data)=>{
          console.log("Add Success ")
          this.categories.push(data)
          this.frmCtg = new Category(null, null)
        },(err) => {
          console.log("Add Error ", err)
        }
      )
    } else{
      this.ctgsvc.modifyCategory(this.frmCtg).subscribe(
        (data)=>{
          console.log("Modify Success", data)
          let idx = this.categories.findIndex((e)=> e.id == data.id)
          this.categories[idx] = data
        }, (err) => {
          console.log("Update Error ", err)
        }
      )
    }
  }

  delete(id:number){
    this.ctgsvc.deleteCategory(id).subscribe(
      (data) => {
        console.log("Delete Succeess ")
        let idx = this.categories.findIndex((e) => e.id == id)
        this.categories.splice(idx, 1)
      }, (err) =>{
        console.log("Delete Error ", err)
      }
    )
  }

  edit(selectedCtg:Category){
    //this.frmCtg = selectedCtg
    Object.assign(this.frmCtg, selectedCtg)    
  }

}
