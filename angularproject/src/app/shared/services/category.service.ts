import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Category } from 'src/app/models/category.model';

@Injectable()
export class CategoryService{
    private restUrl="http://localhost:3000/wscategories"
    constructor(private http:HttpClient){}

    getCategories(){
        return this.http.get<Category[]>(this.restUrl)
    }
    
    addCategory(newctg:Category){
        return this.http.post<Category>(this.restUrl, newctg)
    }

    deleteCategory(id:number){
        return this.http.delete(this.restUrl + "/" + id)
    }

    modifyCategory(modifiedCtg:Category){
        return this.http.put<Category>(this.restUrl+"/"+modifiedCtg.id, modifiedCtg)
    }
}