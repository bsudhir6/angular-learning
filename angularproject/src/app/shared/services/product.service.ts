import { Injectable } from "@angular/core";
import { Product } from 'src/app/models/product.model';

@Injectable()
export class ProductService{
    private productsData:Product[]=[]
    constructor(){
        this.productsData = [new Product(1, "Bravia", 65000, "Sony TV", 1), 
                                new Product(2, "Galaxy S", 14000, "Samsung Mobile", 2), 
                                new Product(1, "iPhone", 123000, "Apple Nex Gen Smart Phone", 4)]
    }

    getProducts(){
        return this.productsData
    }
}