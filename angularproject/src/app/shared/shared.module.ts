import { NgModule } from "@angular/core";
import { MySignComponent } from './mysign.component';
import { SearchDataPipe } from './searchData.pipe';

@NgModule({
    declarations: [MySignComponent, SearchDataPipe],
    //Allows to access components from one module to another.
    exports: [MySignComponent, SearchDataPipe]
})
export class SharedModule{
    constructor(){
        console.log("Shared Module Constructor")
    }
}