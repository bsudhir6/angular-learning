import { Component, Input, EventEmitter, Output, OnDestroy, OnChanges, OnInit } from "@angular/core";

@Component({
    selector: "my-sign",
    template: `<h3>{{title}}</h3>
    <form>
        User Name: <input type="text" placeholder="Enter User Name" #txtUser> <br>
        Password: <input type="password" placeholder="Enter Password" #txtPwd> <br>
        <button class=" btn btn-primary"  type="button" (click)="handleButtonClick(txtUser.value, txtPwd.value)">{{title}}</button>
    </form>
    `
})
export class MySignComponent implements OnDestroy, OnChanges, OnInit{
    @Input()
    title:string;

    @Output()
    myEvent = new EventEmitter();

    handleButtonClick(usr:string, pwd:string){
        console.log("User Name: ", usr, " Password: ", pwd)
        this.myEvent.emit({usr, pwd})
    }

    constructor(){
        console.log("My Sign Constructor", this.title)
    }
    ngOnInit(){
        console.log("My Sign NG On INIT", this.title)
    }

    ngOnChanges(){
        console.log("My Sign NG on Changes")
    }

    ngOnDestroy(){
        console.log("My Sign NG ON Destroy")
    }
}