import { Component } from "@angular/core";

@Component({
    selector: "sign-up",
    template: `<my-sign [title]="signupHeading"></my-sign>`
})
export class SignUpComponent{
    signupHeading = "Quick Sign up"
}