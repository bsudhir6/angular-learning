import { Component, Output } from "@angular/core";
import { LoginService } from '../shared/services/login.service';
import { Router } from '@angular/router';

@Component({
    selector: "sign-in",
    template: `<h4>Welcome {{ userName }}</h4>
                <my-sign [title]="signinHeading" (myEvent)=handleMyEvent($event)></my-sign>`
})

export class SignInComponent{
    userName = "Guest"
    signinHeading = "Sign In"
    
    constructor(private lsvc:LoginService, private router:Router){}
    
    handleMyEvent(obj){
        console.log("User Name is SignIn Component: ", obj.usr, "Pwd in SignIn Component: ", obj.pwd)
        this.userName = obj.usr;
        if(this.lsvc.isValidUser(obj.usr, obj.pwd)){
            this.router.navigate(['/categories'])
        }else{
            this.router.navigate(['login/error'])
        }
    }
}