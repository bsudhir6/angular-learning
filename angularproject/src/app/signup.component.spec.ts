import { TestBed } from "@angular/core/testing";
import { SignUpComponent } from './login/signup.component';
import { MySignComponent } from './shared/mysign.component';
import { LoginService } from './shared/services/login.service';

describe("Verify Signup Component", () => {
    beforeEach(()=>{
        TestBed.configureTestingModule({
            declarations: [SignUpComponent, MySignComponent]
        })
    })
    it("Should verify instance of a component", ()=>{                    
        let f = TestBed.createComponent(SignUpComponent)
        expect(f).toBeDefined();
    })
    it("should verify data member", ()=>{
        let f = TestBed.createComponent(SignUpComponent);
        let obj = f.componentInstance;
        expect(obj.signupHeading).toEqual("Quick Sign Up")
    })
    it("should verify login service", ()=>{
        let ls:LoginService = new LoginService()
        expect(ls.isValidUser("admin", "abc")).toBeTruthy()
    })
})