import { Component } from '@angular/core';

@Component({
    //specify tag name for the component
    selector: "app-root",
    //Output for the Component
    template: `<!--<h3> My First Angular Components </h3>-->
    <app-header></app-header>
    <router-outlet></router-outlet>`
})
export class AppComponent{
    constructor(){
        console.log("App Component Constructor")
    }
}