(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./login/login.module": [
		"./src/app/login/login.module.ts",
		"login-login-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        console.log("App Component Constructor");
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            //specify tag name for the component
            selector: "app-root",
            //Output for the Component
            template: "<!--<h3> My First Angular Components </h3>-->\n    <app-header></app-header>\n    <router-outlet></router-outlet>"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.header.component.ts":
/*!*****************************************!*\
  !*** ./src/app/app.header.component.ts ***!
  \*****************************************/
/*! exports provided: AppHeader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppHeader", function() { return AppHeader; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared/services/login.service */ "./src/app/shared/services/login.service.ts");



var AppHeader = /** @class */ (function () {
    function AppHeader(lsvs) {
        this.lsvs = lsvs;
        console.log("App Header Component Constructor.");
    }
    AppHeader.prototype.isAdminLoggedIn = function () {
        return this.lsvs.getIsAdminLoggedIn();
    };
    AppHeader = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-header",
            // template: "<h4>Welcome to Shopping Site<h4>"
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"]])
    ], AppHeader);
    return AppHeader;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_header_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.header.component */ "./src/app/app.header.component.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _shopping_shopping_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shopping/shopping.module */ "./src/app/shopping/shopping.module.ts");
/* harmony import */ var _menulinks_components__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./menulinks.components */ "./src/app/menulinks.components.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_product_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./shared/services/product.service */ "./src/app/shared/services/product.service.ts");
/* harmony import */ var _shared_services_cart_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./shared/services/cart.service */ "./src/app/shared/services/cart.service.ts");
/* harmony import */ var _shared_services_category_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./shared/services/category.service */ "./src/app/shared/services/category.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _shopping_products_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./shopping/products.component */ "./src/app/shopping/products.component.ts");
/* harmony import */ var _shared_services_login_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./shared/services/login.service */ "./src/app/shared/services/login.service.ts");
/* harmony import */ var _shared_services_login_guard__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./shared/services/login.guard */ "./src/app/shared/services/login.guard.ts");
















var appRoutes = [{ path: "home", component: _menulinks_components__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"] }, { path: "list", component: _menulinks_components__WEBPACK_IMPORTED_MODULE_6__["ListComponent"], children: [
            { path: "", component: _shopping_products_component__WEBPACK_IMPORTED_MODULE_13__["ProductsComponent"] },
            { path: ":ctgId", component: _shopping_products_component__WEBPACK_IMPORTED_MODULE_13__["ProductsComponent"] }
        ] },
    { path: "login", loadChildren: "./login/login.module#LoginModule" },
    { path: "", redirectTo: "home", pathMatch: 'full' }, { path: "**", component: _menulinks_components__WEBPACK_IMPORTED_MODULE_6__["NotFoundComponent"] }];
var AppModule = /** @class */ (function () {
    function AppModule() {
        console.log("App module Constructer.");
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            //Register Components
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _app_header_component__WEBPACK_IMPORTED_MODULE_2__["AppHeader"], _menulinks_components__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"], _menulinks_components__WEBPACK_IMPORTED_MODULE_6__["ListComponent"], _menulinks_components__WEBPACK_IMPORTED_MODULE_6__["NotFoundComponent"]],
            //Startup Component Information
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]],
            //Moduel Dependencies
            imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["BrowserModule"], _shopping_shopping_module__WEBPACK_IMPORTED_MODULE_5__["ShoppingModule"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forRoot(appRoutes, { useHash: true }), _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_12__["HttpClientModule"]],
            // Register Services
            providers: [_shared_services_product_service__WEBPACK_IMPORTED_MODULE_9__["ProductService"], _shared_services_cart_service__WEBPACK_IMPORTED_MODULE_10__["CartService"], _shared_services_category_service__WEBPACK_IMPORTED_MODULE_11__["CategoryService"], _shared_services_login_service__WEBPACK_IMPORTED_MODULE_14__["LoginService"], _shared_services_login_guard__WEBPACK_IMPORTED_MODULE_15__["LoginGuard"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/header.component.html":
/*!***************************************!*\
  !*** ./src/app/header.component.html ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <h4>External Header Componenet Template</h4> -->\r\n<nav class=\"navbar bg-dark navbar-expand-sm navbar-dark\">\r\n<a routerLink=\"/\" class=\"navbar-brand\">My App</a>\r\n<ul class=\"navbar-nav\">\r\n    <li class=\"nav-item\">\r\n        <a routerLink=\"/home\" class=\"nav-link\">Home</a>\r\n    </li>\r\n    <li class=\"nav-item\" *ngIf=\"!isAdminLoggedIn()\">\r\n        <a routerLink=\"login/signin\" class=\"nav-link\">Sign In</a>\r\n    </li>\r\n    <li class=\"nav-item\" *ngIf=\"!isAdminLoggedIn()\">\r\n        <a routerLink=\"login/signup\" class=\"nav-link\">Quick Sign Up</a>\r\n    </li>\r\n    <li class=\"nav-item\">\r\n        <a routerLink=\"/list\" class=\"nav-link\">Shopping List</a>\r\n    </li>\r\n    <li class=\"nav-item\" *ngIf=\"isAdminLoggedIn()\">\r\n            <a routerLink=\"/categories\" class=\"nav-link\">Categories</a>\r\n    </li>\r\n    <li class=\"nav-item\" *ngIf=\"isAdminLoggedIn()\">\r\n            <a routerLink=\"login/signout\" class=\"nav-link\">Sign Out</a>\r\n    </li>    \r\n</ul>\r\n</nav>"

/***/ }),

/***/ "./src/app/menulinks.components.ts":
/*!*****************************************!*\
  !*** ./src/app/menulinks.components.ts ***!
  \*****************************************/
/*! exports provided: HomeComponent, ListComponent, NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_category_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared/services/category.service */ "./src/app/shared/services/category.service.ts");



var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
        this.myinfo = "<u>My Sample Info Content</u>";
        this.count = 0;
    }
    HomeComponent.prototype.incrementCount = function () {
        this.count = this.count + 1;
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-home",
            template: "<h1>Welcome to My Shopping Application</h1>\n                <h3 [innerHTML]=\"myinfo\"></h3>\n                <h4> The Count value is {{ count }}</h4>\n                <input type=\"number\" value=\"{{ count }}\" />\n                <input type=\"number\" [value]=\"count\" />\n                <button class=\"btn btn-primary\" (click) = \"incrementCount()\">Increase</button>\n                <input type=\"number\" [(ngModel)]=\"count\" />"
        })
    ], HomeComponent);
    return HomeComponent;
}());

var ListComponent = /** @class */ (function () {
    function ListComponent(ctgsvc) {
        this.ctgsvc = ctgsvc;
        this.ctgLinks = [];
    }
    ListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.ctgsvc.getCategories().subscribe(function (data) {
            console.log("Success ", data);
            _this.ctgLinks = data;
        }, function (err) {
            console.log("Error ", err);
        });
    };
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-list",
            template: "\n    <div class=\"row\">\n        <div class=\"col-sm-2 card\">\n            <div class=\"card-header bg-info\"><b>Categories</b></div>\n            <div class=\"card-body\">\n                <ul class=\"list-group\">\n                    <li class=\"list-group-item\" *ngFor=\"let link of ctgLinks\">\n                        <a [routerLink]=\"link.id\">{{link.name}}</a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n        <div class=\"col-sm-5\">\n            <!--<app-products></app-products>-->\n            <router-outlet></router-outlet>\n        </div>\n        <div class=\"col-sm-5 card\">\n            <app-cartitems></app-cartitems>\n        </div>\n    </div>    \n    "
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_category_service__WEBPACK_IMPORTED_MODULE_2__["CategoryService"]])
    ], ListComponent);
    return ListComponent;
}());

var NotFoundComponent = /** @class */ (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "not-found",
            template: "<h2> View Not Found</h2>"
        })
    ], NotFoundComponent);
    return NotFoundComponent;
}());



/***/ }),

/***/ "./src/app/models/cartitem.modle.ts":
/*!******************************************!*\
  !*** ./src/app/models/cartitem.modle.ts ***!
  \******************************************/
/*! exports provided: CartItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartItem", function() { return CartItem; });
var CartItem = /** @class */ (function () {
    function CartItem(id, name, price, quantity) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }
    return CartItem;
}());



/***/ }),

/***/ "./src/app/models/category.model.ts":
/*!******************************************!*\
  !*** ./src/app/models/category.model.ts ***!
  \******************************************/
/*! exports provided: Category */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Category", function() { return Category; });
var Category = /** @class */ (function () {
    function Category(id, name) {
        this.id = id;
        this.name = name;
    }
    return Category;
}());



/***/ }),

/***/ "./src/app/models/product.model.ts":
/*!*****************************************!*\
  !*** ./src/app/models/product.model.ts ***!
  \*****************************************/
/*! exports provided: Product */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Product", function() { return Product; });
// export class Product{
//     //1. Define data members
//     id:number
//     name:string
//     price:number
//     description:string
//     //2. Initialize data members
//     constructor(id:number, name:string, price:number, description:string){
//         this.id = id
//         this.name = name
//         this.price = price
//         this.description = description
//     }
// }
var Product = /** @class */ (function () {
    //Define and initialize data members
    function Product(id, name, price, description, categoryId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.categoryId = categoryId;
    }
    return Product;
}());



/***/ }),

/***/ "./src/app/shared/mysign.component.ts":
/*!********************************************!*\
  !*** ./src/app/shared/mysign.component.ts ***!
  \********************************************/
/*! exports provided: MySignComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MySignComponent", function() { return MySignComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MySignComponent = /** @class */ (function () {
    function MySignComponent() {
        this.myEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        console.log("My Sign Constructor", this.title);
    }
    MySignComponent.prototype.handleButtonClick = function (usr, pwd) {
        console.log("User Name: ", usr, " Password: ", pwd);
        this.myEvent.emit({ usr: usr, pwd: pwd });
    };
    MySignComponent.prototype.ngOnInit = function () {
        console.log("My Sign NG On INIT", this.title);
    };
    MySignComponent.prototype.ngOnChanges = function () {
        console.log("My Sign NG on Changes");
    };
    MySignComponent.prototype.ngOnDestroy = function () {
        console.log("My Sign NG ON Destroy");
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], MySignComponent.prototype, "title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], MySignComponent.prototype, "myEvent", void 0);
    MySignComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "my-sign",
            template: "<h3>{{title}}</h3>\n    <form>\n        User Name: <input type=\"text\" placeholder=\"Enter User Name\" #txtUser> <br>\n        Password: <input type=\"password\" placeholder=\"Enter Password\" #txtPwd> <br>\n        <button class=\" btn btn-primary\"  type=\"button\" (click)=\"handleButtonClick(txtUser.value, txtPwd.value)\">{{title}}</button>\n    </form>\n    "
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MySignComponent);
    return MySignComponent;
}());



/***/ }),

/***/ "./src/app/shared/searchData.pipe.ts":
/*!*******************************************!*\
  !*** ./src/app/shared/searchData.pipe.ts ***!
  \*******************************************/
/*! exports provided: SearchDataPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchDataPipe", function() { return SearchDataPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SearchDataPipe = /** @class */ (function () {
    function SearchDataPipe() {
    }
    SearchDataPipe.prototype.transform = function (input, searchVar, searchCol) {
        if (searchVar != undefined && searchCol != undefined && searchVar != "") {
            return input.filter(function (e) { return e[searchCol].indexOf(searchVar) > -1; });
        }
        else {
            return input;
        }
    };
    SearchDataPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: "searchData"
        })
    ], SearchDataPipe);
    return SearchDataPipe;
}());



/***/ }),

/***/ "./src/app/shared/services/cart.service.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/services/cart.service.ts ***!
  \*************************************************/
/*! exports provided: CartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartService", function() { return CartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CartService = /** @class */ (function () {
    function CartService() {
        this.cartData = [];
    }
    CartService.prototype.getCartItems = function () {
        return this.cartData;
    };
    CartService.prototype.addCartItem = function (newItem) {
        this.cartData.push(newItem);
    };
    CartService.prototype.deleteCartItem = function (index) {
        this.cartData.splice(index, 1);
    };
    CartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], CartService);
    return CartService;
}());



/***/ }),

/***/ "./src/app/shared/services/category.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/services/category.service.ts ***!
  \*****************************************************/
/*! exports provided: CategoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryService", function() { return CategoryService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var CategoryService = /** @class */ (function () {
    function CategoryService(http) {
        this.http = http;
        this.restUrl = "http://localhost:3000/wscategories";
    }
    CategoryService.prototype.getCategories = function () {
        return this.http.get(this.restUrl);
    };
    CategoryService.prototype.addCategory = function (newctg) {
        return this.http.post(this.restUrl, newctg);
    };
    CategoryService.prototype.deleteCategory = function (id) {
        return this.http.delete(this.restUrl + "/" + id);
    };
    CategoryService.prototype.modifyCategory = function (modifiedCtg) {
        return this.http.put(this.restUrl + "/" + modifiedCtg.id, modifiedCtg);
    };
    CategoryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], CategoryService);
    return CategoryService;
}());



/***/ }),

/***/ "./src/app/shared/services/login.guard.ts":
/*!************************************************!*\
  !*** ./src/app/shared/services/login.guard.ts ***!
  \************************************************/
/*! exports provided: LoginGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginGuard", function() { return LoginGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.service */ "./src/app/shared/services/login.service.ts");



var LoginGuard = /** @class */ (function () {
    function LoginGuard(lsvs) {
        this.lsvs = lsvs;
    }
    LoginGuard.prototype.canActivate = function () {
        return this.lsvs.getIsAdminLoggedIn();
    };
    LoginGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"]])
    ], LoginGuard);
    return LoginGuard;
}());



/***/ }),

/***/ "./src/app/shared/services/login.service.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/services/login.service.ts ***!
  \**************************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LoginService = /** @class */ (function () {
    function LoginService() {
        this.isAdminLoggedIn = false;
    }
    LoginService.prototype.getIsAdminLoggedIn = function () {
        return this.isAdminLoggedIn;
    };
    LoginService.prototype.setIsAdminLoggedIn = function (newval) {
        this.isAdminLoggedIn = newval;
    };
    LoginService.prototype.isValidUser = function (username, pwd) {
        var flag = false;
        if (username == "admin") {
            flag = true;
        }
        this.isAdminLoggedIn = flag;
        return flag;
    };
    LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/shared/services/product.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/services/product.service.ts ***!
  \****************************************************/
/*! exports provided: ProductService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductService", function() { return ProductService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_models_product_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/models/product.model */ "./src/app/models/product.model.ts");



var ProductService = /** @class */ (function () {
    function ProductService() {
        this.productsData = [];
        this.productsData = [new src_app_models_product_model__WEBPACK_IMPORTED_MODULE_2__["Product"](1, "Bravia", 65000, "Sony TV", 1),
            new src_app_models_product_model__WEBPACK_IMPORTED_MODULE_2__["Product"](2, "Galaxy S", 14000, "Samsung Mobile", 2),
            new src_app_models_product_model__WEBPACK_IMPORTED_MODULE_2__["Product"](1, "iPhone", 123000, "Apple Nex Gen Smart Phone", 4)];
    }
    ProductService.prototype.getProducts = function () {
        return this.productsData;
    };
    ProductService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ProductService);
    return ProductService;
}());



/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _mysign_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mysign.component */ "./src/app/shared/mysign.component.ts");
/* harmony import */ var _searchData_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./searchData.pipe */ "./src/app/shared/searchData.pipe.ts");




var SharedModule = /** @class */ (function () {
    function SharedModule() {
        console.log("Shared Module Constructor");
    }
    SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_mysign_component__WEBPACK_IMPORTED_MODULE_2__["MySignComponent"], _searchData_pipe__WEBPACK_IMPORTED_MODULE_3__["SearchDataPipe"]],
            //Allows to access components from one module to another.
            exports: [_mysign_component__WEBPACK_IMPORTED_MODULE_2__["MySignComponent"], _searchData_pipe__WEBPACK_IMPORTED_MODULE_3__["SearchDataPipe"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/shopping/cartitems.component.html":
/*!***************************************************!*\
  !*** ./src/app/shopping/cartitems.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>Cart Items are: <span class=\"badge badge-primary\">{{cartItems.length}}</span></h3>\n<table class=\"table table-bordered\">\n  <tr>\n    <th>Name</th>\n    <th>Price</th>\n    <th>Quantity</th>\n    <th>Amount</th>\n  </tr>\n  <tr *ngFor=\"let item of cartItems; let i = index\">    \n    <td><button class=\"btn btn-danger btn-sm\" (click)=\"delete(i)\">X</button>{{item.name}}</td>\n    <td>{{item.price}}</td>\n    <td><input type=\"number\" [(ngModel)]=\"item.quantity\" style=\"width:50px\"></td>\n    <td>{{item.price * item.quantity}}</td>\n  </tr>\n  <tr *ngIf=\"cartItems.length == 0\">\n    <th colspan=\"4\">No Data Found.</th>\n  </tr>\n  <tr>    \n    <td colspan=\"3\">Total Amount</td>\n    <td>{{totalAmount()}}</td>\n  </tr>\n</table>"

/***/ }),

/***/ "./src/app/shopping/cartitems.component.ts":
/*!*************************************************!*\
  !*** ./src/app/shopping/cartitems.component.ts ***!
  \*************************************************/
/*! exports provided: CartitemsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartitemsComponent", function() { return CartitemsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/services/cart.service */ "./src/app/shared/services/cart.service.ts");



var CartitemsComponent = /** @class */ (function () {
    function CartitemsComponent(csvc) {
        this.csvc = csvc;
        this.cartItems = [];
    }
    CartitemsComponent.prototype.ngOnInit = function () {
        this.cartItems = this.csvc.getCartItems();
    };
    CartitemsComponent.prototype.delete = function (index) {
        this.csvc.deleteCartItem(index);
    };
    CartitemsComponent.prototype.totalAmount = function () {
        var tot = 0;
        for (var _i = 0, _a = this.cartItems; _i < _a.length; _i++) {
            var e = _a[_i];
            tot += (e.price * e.quantity);
        }
        return tot;
    };
    CartitemsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cartitems',
            template: __webpack_require__(/*! ./cartitems.component.html */ "./src/app/shopping/cartitems.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_cart_service__WEBPACK_IMPORTED_MODULE_2__["CartService"]])
    ], CartitemsComponent);
    return CartitemsComponent;
}());



/***/ }),

/***/ "./src/app/shopping/categories.component.html":
/*!****************************************************!*\
  !*** ./src/app/shopping/categories.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>Manage Categories</h2>\n<form>\n  <input placeholder=\"Category Id\" name=\"txtId\" [(ngModel)]=\"frmCtg.id\" readonly>\n  <input placeholder=\"Enter Category Name\" name=\"txtName\" [(ngModel)]=\"frmCtg.name\">\n  <button class=\"btn btn-primary\" type=\"button\" (click)=\"save()\">Save</button>\n</form>\n<br>\n<table class=\"table table-bordered\">\n  <tr>\n    <th>Id</th>\n    <th>Name</th>\n    <th>Actions</th>\n  </tr>\n  <tr *ngIf=\"categories.length == 0\">\n    <th colspan=\"4\">No Data Found.</th>\n  </tr>\n  <tr *ngFor=\"let ctg of categories\">\n    <td>{{ctg.id}}</td>\n    <td>{{ctg.name}}</td>\n    <td><button class=\"btn btn-danger\" (click)=\"delete(ctg.id)\">Delete</button>\n      <button class=\"btn btn-success\" (click)=\"edit(ctg)\">Edit</button>\n    </td>\n  </tr>\n</table>"

/***/ }),

/***/ "./src/app/shopping/categories.component.ts":
/*!**************************************************!*\
  !*** ./src/app/shopping/categories.component.ts ***!
  \**************************************************/
/*! exports provided: CategoriesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesComponent", function() { return CategoriesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_category_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/category.model */ "./src/app/models/category.model.ts");
/* harmony import */ var _shared_services_category_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/services/category.service */ "./src/app/shared/services/category.service.ts");




var CategoriesComponent = /** @class */ (function () {
    function CategoriesComponent(ctgsvc) {
        this.ctgsvc = ctgsvc;
        this.categories = [];
        this.frmCtg = new _models_category_model__WEBPACK_IMPORTED_MODULE_2__["Category"](null, null);
    }
    CategoriesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.ctgsvc.getCategories().subscribe(function (data) { return _this.categories = data; }, function (err) { return console.log("Error", err); });
    };
    CategoriesComponent.prototype.save = function () {
        var _this = this;
        if (this.frmCtg.id == null) {
            this.ctgsvc.addCategory(this.frmCtg).subscribe(function (data) {
                console.log("Add Success ");
                _this.categories.push(data);
                _this.frmCtg = new _models_category_model__WEBPACK_IMPORTED_MODULE_2__["Category"](null, null);
            }, function (err) {
                console.log("Add Error ", err);
            });
        }
        else {
            this.ctgsvc.modifyCategory(this.frmCtg).subscribe(function (data) {
                console.log("Modify Success", data);
                var idx = _this.categories.findIndex(function (e) { return e.id == data.id; });
                _this.categories[idx] = data;
            }, function (err) {
                console.log("Update Error ", err);
            });
        }
    };
    CategoriesComponent.prototype.delete = function (id) {
        var _this = this;
        this.ctgsvc.deleteCategory(id).subscribe(function (data) {
            console.log("Delete Succeess ");
            var idx = _this.categories.findIndex(function (e) { return e.id == id; });
            _this.categories.splice(idx, 1);
        }, function (err) {
            console.log("Delete Error ", err);
        });
    };
    CategoriesComponent.prototype.edit = function (selectedCtg) {
        //this.frmCtg = selectedCtg
        Object.assign(this.frmCtg, selectedCtg);
    };
    CategoriesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-categories',
            template: __webpack_require__(/*! ./categories.component.html */ "./src/app/shopping/categories.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_category_service__WEBPACK_IMPORTED_MODULE_3__["CategoryService"]])
    ], CategoriesComponent);
    return CategoriesComponent;
}());



/***/ }),

/***/ "./src/app/shopping/products.component.html":
/*!**************************************************!*\
  !*** ./src/app/shopping/products.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>List of Products.</h3>\n{{myDate | date:'MM/dd/yyyy'}}\n<br>\n<div>\n  Search: <input type=\"text\" [(ngModel)]='queryString'>\n</div>\n<div class=\"card-deck\">\n  <div class=\"card\" *ngFor=\"let product of products | searchData:queryString: 'name'\">\n    <div class=\"card-header bg-warning\">\n      <b>{{product.name | uppercase}}</b>\n    </div>\n    <div class=\"card-body\">\n      Price: {{product.price | currency:'INR'}}\n      <br>Description: {{product.description}}\n    </div>\n    <div class=\"card-footer\">\n      <button class=\"btn btn-primary\" (click)=\"addToCart(product)\">\n        Add To Cart\n      </button>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/shopping/products.component.ts":
/*!************************************************!*\
  !*** ./src/app/shopping/products.component.ts ***!
  \************************************************/
/*! exports provided: ProductsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsComponent", function() { return ProductsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/services/product.service */ "./src/app/shared/services/product.service.ts");
/* harmony import */ var _shared_services_cart_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/services/cart.service */ "./src/app/shared/services/cart.service.ts");
/* harmony import */ var _models_cartitem_modle__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/cartitem.modle */ "./src/app/models/cartitem.modle.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var ProductsComponent = /** @class */ (function () {
    function ProductsComponent(psvc, csvc, ar) {
        this.psvc = psvc;
        this.csvc = csvc;
        this.ar = ar;
        this.myDate = new Date();
        this.products = [];
        this.queryString = "";
    }
    ProductsComponent.prototype.addToCart = function (selectedProduct) {
        var myItem = new _models_cartitem_modle__WEBPACK_IMPORTED_MODULE_4__["CartItem"](selectedProduct.id, selectedProduct.name, selectedProduct.price, 1);
        this.csvc.addCartItem(myItem);
    };
    ProductsComponent.prototype.ngOnInit = function () {
        var _this = this;
        //this.products = this.psvc.getProducts()
        this.ar.params.subscribe(function (paramdata) {
            var paramId = paramdata.ctgId;
            if (paramId == undefined) {
                _this.products = _this.psvc.getProducts();
            }
            else {
                _this.products = _this.psvc.getProducts().filter(function (e) { return e.categoryId == paramId; });
            }
        });
    };
    ProductsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-products',
            template: __webpack_require__(/*! ./products.component.html */ "./src/app/shopping/products.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_services_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _shared_services_cart_service__WEBPACK_IMPORTED_MODULE_3__["CartService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], ProductsComponent);
    return ProductsComponent;
}());



/***/ }),

/***/ "./src/app/shopping/shopping.module.ts":
/*!*********************************************!*\
  !*** ./src/app/shopping/shopping.module.ts ***!
  \*********************************************/
/*! exports provided: ShoppingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingModule", function() { return ShoppingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _products_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./products.component */ "./src/app/shopping/products.component.ts");
/* harmony import */ var _cartitems_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cartitems.component */ "./src/app/shopping/cartitems.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _categories_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./categories.component */ "./src/app/shopping/categories.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_login_guard__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/services/login.guard */ "./src/app/shared/services/login.guard.ts");










var ShoppingModule = /** @class */ (function () {
    function ShoppingModule() {
        console.log("Shopping Module Constructor.");
    }
    ShoppingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_products_component__WEBPACK_IMPORTED_MODULE_2__["ProductsComponent"], _cartitems_component__WEBPACK_IMPORTED_MODULE_3__["CartitemsComponent"], _categories_component__WEBPACK_IMPORTED_MODULE_7__["CategoriesComponent"]],
            exports: [_products_component__WEBPACK_IMPORTED_MODULE_2__["ProductsComponent"], _cartitems_component__WEBPACK_IMPORTED_MODULE_3__["CartitemsComponent"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"], _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forChild([{ path: "categories", component: _categories_component__WEBPACK_IMPORTED_MODULE_7__["CategoriesComponent"], canActivate: [_shared_services_login_guard__WEBPACK_IMPORTED_MODULE_9__["LoginGuard"]] }])]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ShoppingModule);
    return ShoppingModule;
}());



/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");


Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_0__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_1__["AppModule"]);
console.log("Angular Entry File Main TS Message");


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Repositories\angular-learning\angularproject\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map