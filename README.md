# Angular Learning

Trainer name: satyamtd@gmail.com
Contact: 9440220062

## Why Angular?

Angular is mainly used for Web Application Development.

## Environment Setup and Check

Check node js installed or not

> node -v

install angular cli

> npm install -g @angular/cli

Clean NPM Cache:

> npm cache clean --force

check angular cli installed or not.

> ng version

Install JSON Server - Helper tool for creating web service

> npm install -g json-server

Check JSON-Server Version:

> json-server -v

##TypeScript

website: typescriptlang.org

TypeScript allows us to add data type to variables and function declarations. --> Strongly typed language.
    1. ES6 support
    2. Strongly Typed
    3. Class-Based and Object-Oriented
    4. Transpiles to JavaScript

var keyword indicates global variable in typescript. should not use var keyword in loops etc.

    1. Type Declaration
       1. Implicit Type Declaration - internally managed by language    - also called Type Inference     
       2. Explicit Type Declaration - type should be defined by developer/ - also called Type Annotation
        > identifier : datatype
    2. Function Declaration
        function sum(a:number , b:number):number{
            return a+b;
        }
    3. To define constructor in a class use Constructor function. It does not match with Class's name.
    4. Create an object for class
        > let e1:Emp = new Emp()
    5. Each TypeScript file considered as module.
    6. Decorator Keyword: meta data for any type of data (class information, method information) in typescript files. This is also a function. Decorator function prefixed with an @

    Data Types:
        1. Primitive Types: string, number, boolean
        2. Special Types: any, void
        3. Array Types: [] / Array
        4. Function Types: Function
        5. Object Types: {}
        6. Custome Types: Class, Interface, Enum
    
    Export & import keywords
           1. export - allow member of module to be accessed from other file.
           2. import - declare the member of another module to current file.
           3. from - to refer to module from where member is being accessed

        export example:

        export class Emp{

        }

        import example:

        import {}

## About Angular2+

    1. Implemented in TypesScript
    2. Can create Cross Platform Apps - web, mobile, ddesktop
    3. It's a framework to develop Component based UI Development (allows us to create new tags to extend the HTML) - DOM Manipulation
    4. Implements many Design Patterns
       1. MVC Pattern
       2. Singleton
       3. Dependency Injection
    5. Angular API is available in form of libraries.
       1. @angular/common
       2. @angular/forms
       3. @angular/core

## Setup Angular2+ Project

Two ways to setup Angular2+ project

    1. Manual Setup - need to setup all required files for angular project
    2. Ready To Code Project Setup - Ready made project setup available for The Command Line Interface

Angular CLI Commands:
    1. new - create a new angular project
    2. serve - to execute angular application
    3. test - to do unit test and end to end test on angular application
    4. build - to make angular application into deployable app.


## Create a new Project

Command to create new Project:

> ng new angularproject

Output:

    C:\Repositories\angular-learning (master -> origin)
    λ ng new angularproject
    ? Would you like to add Angular routing? No
    ? Which stylesheet format would you like to use? CSS
    CREATE angularproject/angular.json (3879 bytes)
    CREATE angularproject/package.json (1313 bytes)
    CREATE angularproject/README.md (1031 bytes)
    CREATE angularproject/tsconfig.json (435 bytes)
    CREATE angularproject/tslint.json (1621 bytes)
    CREATE angularproject/.editorconfig (246 bytes)
    CREATE angularproject/.gitignore (629 bytes)
    CREATE angularproject/src/favicon.ico (5430 bytes)
    CREATE angularproject/src/index.html (301 bytes)
    CREATE angularproject/src/main.ts (372 bytes)
    CREATE angularproject/src/polyfills.ts (2841 bytes)
    CREATE angularproject/src/styles.css (80 bytes)
    CREATE angularproject/src/test.ts (642 bytes)
    CREATE angularproject/src/browserslist (388 bytes)
    CREATE angularproject/src/karma.conf.js (1027 bytes)
    CREATE angularproject/src/tsconfig.app.json (166 bytes)
    CREATE angularproject/src/tsconfig.spec.json (256 bytes)
    CREATE angularproject/src/tslint.json (244 bytes)
    CREATE angularproject/src/assets/.gitkeep (0 bytes)
    CREATE angularproject/src/environments/environment.prod.ts (51 bytes)
    CREATE angularproject/src/environments/environment.ts (662 bytes)
    CREATE angularproject/src/app/app.module.ts (314 bytes)
    CREATE angularproject/src/app/app.component.html (1120 bytes)
    CREATE angularproject/src/app/app.component.spec.ts (1002 bytes)
    CREATE angularproject/src/app/app.component.ts (218 bytes)
    CREATE angularproject/src/app/app.component.css (0 bytes)
    CREATE angularproject/e2e/protractor.conf.js (752 bytes)
    CREATE angularproject/e2e/tsconfig.e2e.json (213 bytes)
    CREATE angularproject/e2e/src/app.e2e-spec.ts (643 bytes)
    CREATE angularproject/e2e/src/app.po.ts (251 bytes)

    > node-sass@4.11.0 install C:\Repositories\angular-learning\angularproject\node_modules\node-sass
    > node scripts/install.js

    Downloading binary from https://github.com/sass/node-sass/releases/download/v4.11.0/win32-x64-64_binding.node
    Download complete  ] - :
    Binary saved to C:\Repositories\angular-learning\angularproject\node_modules\node-sass\vendor\win32-x64-64\binding.node
    Caching binary to C:\Users\Sudhir\AppData\Roaming\npm-cache\node-sass\4.11.0\win32-x64-64_binding.node

    > node-sass@4.11.0 postinstall C:\Repositories\angular-learning\angularproject\node_modules\node-sass
    > node scripts/build.js

    Binary found at C:\Repositories\angular-learning\angularproject\node_modules\node-sass\vendor\win32-x64-64\binding.node
    Testing binary
    Binary is fine
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.8 (node_modules\fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.8: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})

    added 1086 packages from 1026 contributors and audited 42611 packages in 292.738s
    found 1 high severity vulnerability
    run `npm audit fix` to fix them, or `npm audit` for details
        Directory is already under version control. Skipping initialization of git.

To Execute Project

> ng serve

output:

    C:\Repositories\angular-learning\angularproject (master -> origin)
    λ ng serve
    ** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **

    Date: 2019-04-22T06:26:12.786Z
    Hash: b16a2db7a1cb226b8d75
    Time: 16492ms
    chunk {es2015-polyfills} es2015-polyfills.js, es2015-polyfills.js.map (es2015-polyfills) 284 kB [initial] [rendered]
    chunk {main} main.js, main.js.map (main) 9.87 kB [initial] [rendered]
    chunk {polyfills} polyfills.js, polyfills.js.map (polyfills) 236 kB [initial] [rendered]
    chunk {runtime} runtime.js, runtime.js.map (runtime) 6.08 kB [entry] [rendered]
    chunk {styles} styles.js, styles.js.map (styles) 16.3 kB [initial] [rendered]
    chunk {vendor} vendor.js, vendor.js.map (vendor) 3.52 MB [initial] [rendered]
    i ｢wdm｣: Compiled successfully.

Types of Components:
    1. Container Component
    2. Presentation Component

Divide program into multiple modules
    1. Container Module
    2. Presentation Module
    3. Cart Module
    4. Search Module
    5. Login Module etc.

Module Name Convension:

    1. For Login module - Login.module.ts
    2. For application module - Application.module.ts

Angular always loads dependencies first.

## Interpolation Markup Language

To give calcualted outputs in the components use Interpolation Markups

{{ expression }}

can be used with numbers calculation, methods and members of the class.

## Data Binding, Event Binding

> Data Binding [html attribute] = class member
> Event Binding (html event) = class member method to be invoked.

Data Binding Feature - Automatic Synchronization

Routes Definitions:

forRoot()
forChild()

## Working with Child Components
Angular allows nesting
Outer Component is Parent Component
Inner Component Child Component


Defining and Initializing Model Classes

    export class Product{
        //Define and initialize data members
        public constructor(public id:number, public name:string, public price:number, public description:string){ }
    }

## Pipes

Angular provides you with the pipes mechanism to format or transform the data in the template.
The general syntax for using pipe is:

> {{ expression | pipeName: inputParam}}

Two Types:
1. Built-in
2. Custom